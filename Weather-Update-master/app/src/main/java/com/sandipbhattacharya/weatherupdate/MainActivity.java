package com.sandipbhattacharya.weatherupdate;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    EditText etCity, etCountry;
    TextView tvResult;
    Button btn;
    String city;
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etCity = findViewById(R.id.etCity);

        btn = findViewById(R.id.btnGet);
        tvResult=findViewById(R.id.tvResult);
        String api_key = "6373f1a993bc63b1329e0400170e93d2";

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                city = etCity.getText().toString();
                if (!city.equals(""))
                    Toast.makeText(MainActivity.this, "pressed", Toast.LENGTH_LONG).show();

                    url = "http://api.weatherstack.com/current?access_key=6373f1a993bc63b1329e0400170e93d2&query=" + city;
                get_data();
            }
        });
    }

    private void get_data()
    {
        Toast.makeText(MainActivity.this, "InGetData", Toast.LENGTH_LONG).show();

        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String out="";
                try {
                    Toast.makeText(MainActivity.this, "Intry", Toast.LENGTH_LONG).show();

                    JSONObject object = response.getJSONObject("request");
                    JSONObject current = response.getJSONObject("current");
                    //JSONArray array=response.getJSONArray("weather_descriptions");
                    //JSONObject object1=current.getJSONObject(0);
                    String que1 = object.getString("query");
                    String desc = current.getString("weather_descriptions");
                    String temperature = current.getString("temperature");
                    String wind_speed = current.getString("wind_speed");
                    String wind_dir = current.getString("wind_dir");
                    String precip = current.getString("precip");
                    String humidity = current.getString("humidity");
                    out+="City: " + que1;
                    out+="\nDescription: " + desc;
                    out+="\nTemperature: " + temperature + "C";
                    out+="\nWind Speed: " + wind_speed;
                    out+="\nWind direction: " + wind_dir;
                    out+="\nPrecip: " + precip;
                    out+="\nHumidity: " + humidity;

                    tvResult.setText(out);
                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    Toast.makeText(MainActivity.this, "InCatch", Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(MainActivity.this, "InEror", Toast.LENGTH_LONG).show();

            }
        });
        queue.add(request);
    }
}